﻿using System.Collections.Generic;
using UnityEngine;

//Pooling System for several pools within a scene
public class SeveralObjectPools : MonoBehaviour
{
    //Stores pools with the prefabs as a key and Queue of the objects as value
    private Dictionary<string, Queue<GameObject>> severalObjectPools = new Dictionary<string, Queue<GameObject>>();

    //Simple Singelton
    public static SeveralObjectPools Instance { get; private set; }
    private void Awake()
    {
        Instance = this;
    }
    //Simple Singelton

    //The prefabs are requested
    public GameObject Get(GameObject gameObject)
    {
        //Checks dictionary if key matching to the requested prefab can be found
        if (severalObjectPools.TryGetValue(gameObject.name, out Queue<GameObject> objectList))
        {
            //If nothing is in the Queue a new bullet will be instantiated
            if (objectList.Count == 0)
            {
                AddObjects(gameObject);
            }

            //If there are bullets in the Queue we will pull one out of the queue
            return objectList.Dequeue();
        }
        else
        {
            return AddObjects(gameObject);
        }
    }

    //A new object gets created with the prefab, given the name of the prefab and returned
    private GameObject AddObjects(GameObject gameObject)
    {
        GameObject bulletShots = Instantiate(gameObject);
        bulletShots.name = gameObject.name;
        return bulletShots;        
    }

    public void ReturnGameObject(GameObject gameObject)
    {
        //Checks for a matching Key in the dictonary and enqueues the object in the corresponding queue
        if (severalObjectPools.TryGetValue(gameObject.name, out Queue<GameObject> objectList))
        {
            objectList.Enqueue(gameObject);
        }
        else
        {
            //If matching key can't be found, a new queue is created, the object is added and the queue is added to the dictionary
            Queue<GameObject> newObjectQueue = new Queue<GameObject>();
            newObjectQueue.Enqueue(gameObject);
            severalObjectPools.Add(gameObject.name, newObjectQueue);
        }

        gameObject.SetActive(false);
    }
}
