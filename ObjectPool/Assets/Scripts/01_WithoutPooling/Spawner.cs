﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//Simple script to spawn bullets
//This script is attached to the spawner
public class Spawner : MonoBehaviour
{
    [SerializeField]
    private BulletShotWithoutPool bulletPrefab;

    [SerializeField]
    private float refireRate = 0.5f;

    private float fireTimer = 0;

    private void Update()
    {
        fireTimer += Time.deltaTime;

        //As soon as the fire time is higher or equal to the refire rate the timer gets set to 0 and a new bullet gets spawned
        if (fireTimer >= refireRate)
        {
            fireTimer = 0;
            Fire();
        }
    }

    //A new bullet gets instantiated at the position of the spawner
    private void Fire()
    {
        BulletShotWithoutPool shot = Instantiate(bulletPrefab, transform.position, transform.rotation);
    }
}
