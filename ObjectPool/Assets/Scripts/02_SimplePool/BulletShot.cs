﻿using UnityEngine;
using System.Collections;

//Script for the bullets
//Attached to the bullet prefab
public class BulletShot : MonoBehaviour
{
    [SerializeField]
    private float moveSpeed = 10f;

    private float lifeTime;
    private float maxLifeTime = 3f;

    //As the objects are created their life time is 0
    private void OnEnable()
    {
        lifeTime = 0f;
    }

    private void Update()
    {
        //Moves the bullets and updates the life time
        transform.Translate(Vector3.right * moveSpeed * Time.deltaTime);
        lifeTime += Time.deltaTime;

        //As soon as the life time is higher than the maximal life time the bullets will be deactived and put back into the queue
        if (lifeTime > maxLifeTime)
        {
            ShotWithPool.Instance.ReturnToPool(this);
        }
    }
}
