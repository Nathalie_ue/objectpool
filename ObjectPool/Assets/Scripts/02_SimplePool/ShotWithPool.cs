﻿using UnityEngine;
using System.Collections.Generic;

//Pooling System
public class ShotWithPool : MonoBehaviour
{
    [SerializeField]
    private BulletShot bulletPrefab;

    private Queue<BulletShot> bulletShots = new Queue<BulletShot>();
    
    //Simple Singelton
    public static ShotWithPool Instance { get; private set; }
    private void Awake()
    {
        Instance = this;
    }
    //Simple Singelton

    public BulletShot Get()
    {
        //If nothing is in the Queue a new bullet will be instantiated
        if (bulletShots.Count == 0)
        {
            AddShots();
        }

        //If there are bullets in the Queue we will pull one out of the queue
        return bulletShots.Dequeue();
    }

    //To add new bullets to the queue a new object gets created with the prefab, set to false and added to the queue
    private void AddShots()
    {
        BulletShot bulletShot = Instantiate(bulletPrefab);
        bulletShot.gameObject.SetActive(false);
        bulletShots.Enqueue(bulletShot);
    }

    //Put the object back into the queue if not needed in the scene anymore
    public void ReturnToPool (BulletShot bulletShot)
    {
        bulletShot.gameObject.SetActive(false);
        bulletShots.Enqueue(bulletShot);
    }
}
