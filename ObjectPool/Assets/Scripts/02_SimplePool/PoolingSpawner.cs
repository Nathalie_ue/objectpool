﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//Script working with simple Object Pooling
//Script is attached to the spawner
public class PoolingSpawner : MonoBehaviour
{    
    [SerializeField]
    private float refireRate = 0.5f;

    private float fireTimer = 0;

    private void Update()
    {
        fireTimer += Time.deltaTime;

        //As soon as the fire time is higher or equal to the refire rate the timer gets set to 0 and a new bullet gets spawned
        if (fireTimer >= refireRate)
        {
            fireTimer = 0;
            Fire();
        }
    }

    //A new bullet gets set to the position of the spawner and gets activated
    private void Fire()
    {
        BulletShot shot = ShotWithPool.Instance.Get();
        shot.transform.rotation = transform.rotation;
        shot.transform.position = transform.position;
        shot.gameObject.SetActive(true);
    }
}
